<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::group(['middleware' => 'pageAjax'], function() {

    Route::group(['middleware' => 'auth'], function() {
        
        Route::get('/profile', 'ProfileController@index')->name("profile");
        Route::get('/profile/{user}', 'ProfileController@show')->name("profile.show");

        /*
          add when necessary
          Route::group(['prefix' => 'admin', 'middleware' => 'Admin'], function()
          {
          $a = 'admin.';
          Route::get('/', ['as' => $a . 'home', 'uses' => 'AdminController@index']);

          });
         */
    });
    Route::group(['namespace' => 'Auth', 'middleware' => 'guest'], function() {

        Route::get('/social/redirect/{provider}', 'SocialLoginController@redirect');
        Route::get('/social/handle/{provider}', 'SocialLoginController@handle');
    });

    Auth::routes();

    Route::get('/', 'HomeController@index')->name("home");
});

Route::get('/navbar', function (){
    return view('partials.navbar');
});