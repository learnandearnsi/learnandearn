<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use App\User;
use App\Role;
use App\SocialLogin;


class SocialLoginController extends Controller
{
    public function redirect( $provider )
    {

        $providerKey = \Config::get('services.' . $provider);

        if (empty($providerKey)) {

            abort(404,"Provider not found");

        }
        return Socialite::driver( $provider )->redirect();

    }

    public function handle(Request $request, $provider)
    {
        
        if ($request->input('denied') != '') {

            return redirect()->to('/login')
                ->with('status', 'danger')
                ->with('message', 'You did not share your profile data with our social app.');

        }
        
        \Log::debug($provider);
        
        $user = \Socialite::driver( $provider )->user();

        $socialUser = null;

        //Check is this email present
        $userCheck = User::where('email', '=', $user->email)->first();

        $email = $user->email;

        if (!$user->email) {
            $email = 'missing' . str_random(20);
        }

        if (!empty($userCheck)) {

            $socialUser = $userCheck;

        }
        else {

            $sameSocialId = SocialLogin::where('social_id', '=', $user->id)
                ->where('provider', '=', $provider )
                ->first();

            if (empty($sameSocialId)) {

                //There is no combination of this social id and provider, so create new one
                $newSocialUser = new User;
                $newSocialUser->email              = $email;
                $name = explode(' ', $user->name);

                
                $newSocialUser->first_name = $name[0];
                

                
                $newSocialUser->last_name = implode(" ", array_slice($name, 1)); //ADD SPLICE
                

                $newSocialUser->password = bcrypt(str_random(16));
                $newSocialUser->token = str_random(64);
                $newSocialUser->save();

                $socialData = new SocialLogin;
                $socialData->social_id = $user->id;
                $socialData->provider= $provider;
                $newSocialUser->socialLogin()->save($socialData);

                // Add role
                $role = Role::whereName('user')->first();
                $newSocialUser->assignRole($role);
		\Log::debug("user",[$newSocialUser->socialLogin]);
                $socialUser = $newSocialUser;

            }
            else {

                //Load this existing social user
                $socialUser = $sameSocialId->user;

            }

        }
	\Log::debug('user to login', [$socialUser]);
        auth()->login($socialUser, true);

        

        if ( auth()->user()->hasRole('user')) {

            return redirect()->route('home');

        }

        if ( auth()->user()->hasRole('admin')) {

            return redirect()->route('home');
            //return redirect()->route('admin.home');

        }

	\Log::info("User has no role");
	\Log::debug('User:', [auth()->user()]);
	\Log::debug('Role:', [auth()->user()->roles]);

        return abort(500, 'User has no Role assigned, role is obligatory! You did not seed the database with the roles.');
        

    }
}
