<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //
    
    public function index(){
    
        return $this->show(\Auth::user());
        
        
    }
    
    public function show(User $user){
    
        return view('profile.index', compact('user'));
        
        
    }
}
