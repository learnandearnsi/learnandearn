<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];
    
    public function handle($request, \Closure $next)
    {
        logger("Request",["re" => request(),"session" => session(), "token" => csrf_token()]);
        
        return parent::handle($request, $next);
    }
}
