<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request;

class AjaxPageServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(/* Illuminate\Http\Request $request */) {
        //

        view()->composer("*", function ($view) {
            $_ajax = request("_ajax", false);
            $_vue = request("_vue", false);
            $view->with(compact('_ajax','_vue'));
        });
        
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
