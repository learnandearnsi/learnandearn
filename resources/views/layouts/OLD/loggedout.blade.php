@extends((boolval($_ajax) ? 'layouts.ajax_master' : 'layouts.master'))


    
@section('master_title')
    @yield('title')
@endsection


@section('master_includes_css')
    
    @include('partials.chat.includes_css')
    @yield('includes_css')
    
@endsection





@section('master_navbar')

    @yield('navbar')


@endsection

@section('master_content')

    @yield('content')

@endsection


@section('master_footer')

    @include('partials.footer')

@endsection


@section('master_end_of_page_content')

    @include('partials.chat.holder')
    @yield('end_of_page_content')

@endsection

@section('master_end_of_body_content')

    @yield('end_of_body_content')

@endsection


@section('master_includes_js')
    
    @include('partials.chat.includes_js')
    @yield('includes_js')

@endsection