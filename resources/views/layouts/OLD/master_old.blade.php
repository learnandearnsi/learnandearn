

@if(boolval($_ajax))
<root>
<content>
    @yield('content')
</content>
<data>
    {{ json_encode($_viewData)  }}


</data>
<d-navbar>
    @include('partials.navbar')
</d-navbar>
<d-footer>
    @include('partials.footer')
</d-footer>

<d-endofpage>
    @if($_viewData['authenticated'])
    @include('partials.chat.holder')
    @endif
</d-endofpage>

</root>

@else
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @include('partials.favicon')
        <title>{{ isset($_viewData['title']) ? $_viewData['title']." - ": ""  }}Learn and Earn</title>

        <link rel="stylesheet" href="{{ asset('css/theme.css') }}"/>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}"/>
        <link rel="stylesheet" href="{{ asset('css/adjust-width.css') }}"/>
        <link rel="stylesheet" href="{{ asset('css/navbar.css') }}"/>


        <script>
            window._csrfToken = '{!! csrf_token() !!}';
        </script>
    </head>
    <body>
        <div class="page" id="root">
            @include ('partials.navbar')


            <div class="container main-container">

                @yield('content')

            </div>

            @include('partials.footer')
            @if($_viewData['authenticated'])
            @include('partials.chat.holder')
            @endif
        </div>


<!--        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>-->
        <script src="{{ asset('js/app.js') }}"></script>



    </body>
</html>

@endif