@extends('layouts.master')

<?php

$_viewDataChild = $_viewData;

$_viewData = [
    'authenticated' => true,
    'title' => $_viewDataChild['title'],
    'includes' => [
        'css' => [
            'child' => $_viewDataChild['includes']['css'],
            'general' => [
            ],
        ],
        'js' => [
            'child' => $_viewDataChild['includes']['js'],
            'general' => [
            ],
        ]
    ]
        ]
?>



