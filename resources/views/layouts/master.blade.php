<?php
$_data = [
    "csrfToken" => csrf_token(),
    "authenticated" => isset($_viewData['authenticated']) ? $_viewData['authenticated'] : false,
    "title" => (isset($_viewData['title']) ? $_viewData['title'] . " - " : "") . "Learn and Earn",
    "routeName" => isset($_viewData['routeName']) ? $_viewData['routeName'] : ""
];
?>

@if(boolval($_ajax))
<root>
<content>
    @yield('content')
</content>
<data>
    {{ json_encode($_data)  }}


</data>
<d-navbar>
    @include('partials.navbar')
</d-navbar>
<d-footer>
    @include('partials.footer')
</d-footer>

<d-endofpage>
    @if($_viewData['authenticated'])
    @include('partials.chat.holder')
    @endif
</d-endofpage>

</root>


@else
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @include('partials.favicon')
        <title>{!! $_data['title'] !!}</title>

        <!--<link rel="stylesheet" href="{{ asset('css/theme.css') }}"/>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}"/>
        <link rel="stylesheet" href="{{ asset('css/adjust-width.css') }}"/>-->
        <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700"/>
        <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons"/>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="//rawgit.com/FezVrasta/bootstrap-material-design/master/dist/css/bootstrap-material-design.css"/>
        <link rel="stylesheet" href="//rawgit.com/FezVrasta/bootstrap-material-design/master/dist/css/ripples.css"/>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css"/>


        <script>
            window._data = JSON.parse('{!! json_encode($_data) !!}');


        </script>
    </head>
    <body>
        <div class="page" id="root">
            @include ('partials.navbar')

            <div class="container main-container {{ $_data['routeName'] }}">
                @yield('content')
            </div>
            <div>
                @include('partials.footer')
                @if($_data['authenticated'])
                @include('partials.chat.holder')
                @endif
            </div>
            <!--
            <script src="//rawgit.com/FezVrasta/bootstrap-material-design/master/dist/js/ripples.js"></script>
            <script src="//rawgit.com/FezVrasta/bootstrap-material-design/master/dist/js/material.js"></script>
            <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>-->

            <script src="{{ asset('js/manifest.js') }}"></script>
            <script src="{{ asset('js/vendor.js') }}"></script>
            <script src="{{ asset('js/app.js') }}"></script>




    </body>
</html>

@endif