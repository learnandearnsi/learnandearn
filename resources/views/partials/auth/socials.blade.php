<div style="
     position: relative;
     text-align: center;
     margin-bottom: -37px;
     ">
    <hr>

    <span class="text" style="
          position: relative;
          top: -36px;
          padding: 5px;
          background-color: white;
          font-size: 16px;
          font-style: italic;
          /* left: 50%; */
          /* margin-right: auto; */
          /* display: inline-block; */
          color: #bbb;
          ">
        OR
    </span>
</div>
<div class="form-group">
    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
        <a class="btn btn-block btn-danger btn-raised col-md-5 prevent-redirect" href="">
            Google
        </a>
        <a class="btn btn-block btn-info btn-raised col-md-5 prevent-redirect" href="/social/redirect/facebook" >
            Facebook
        </a>
        <a class="btn btn-block btn-info btn-raised col-md-5 prevent-redirect" href="">
            Twitter
        </a>
        <a class="btn btn-block btn-success btn-raised col-md-5 prevent-redirect" href="">
            Github
        </a>
    </div>
</div>
