<?php
$_viewData = [
    'authenticated' => false,
    'title' => 'Login',
    'includes' => [
        'css' => [
        ],
        'js' => [
        ]
    ]
];
?>

@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
            <div class="well">

                <form class="" role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <fieldset>
                        <legend>Login</legend>


                        <div class="form-group label-floating{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">E-Mail Address</label>


                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif

                        </div>

                        <div class="form-group label-floating{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="control-label">Password</label>

                            <input id="password" type="password" class="form-control" name="password" required>
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group">

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>

                        </div>

                        <div class="form-group">

                            <button type="submit" class="btn btn-primary btn-raised">
                                Login
                            </button>

                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                Forgot Your Password?
                            </a>

                        </div>
                        

                        @include('partials.auth.socials')
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
