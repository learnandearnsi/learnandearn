<?php
$_viewData = [
    'authenticated' => false,
    'title' => 'Register',
    'includes' => [
        'css' => [
        ],
        'js' => [
        ]
    ]
];
?>

@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
            <div class="well">

                <form class="" role="form" method="POST" action="{{ route('register') }}">

                    {{ csrf_field() }}
                    <fieldset>
                        <legend>Register</legend>
                        <div class="form-group label-floating{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name" class="control-label">First name</label>


                            <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>

                            @if ($errors->has('first_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                            @endif

                        </div>

                        <div class="form-group label-floating{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="control-label">Last name</label>


                            <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>

                            @if ($errors->has('last_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                            @endif

                        </div>

                        <div class="form-group label-floating{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">E-Mail Address</label>

                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif


                        </div>

                        <div class="form-group label-floating{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="control-label">Password</label>


                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif

                        </div>

                        <div class="form-group label-floating">
                            <label for="password-confirm" class="control-label">Confirm Password</label>


                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                        </div>

                        <div class="form-group">

                            <button type="submit" class="btn btn-primary btn-raised">
                                Register
                            </button>
                            <a class="btn btn-link" href="{{ route('login') }}">
                                Already have an account?
                            </a>
                        </div>


                        @include('partials.auth.socials')
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
