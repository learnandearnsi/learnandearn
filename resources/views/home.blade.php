@extends('layouts.master')
<?php
$_viewData = [
    'authenticated' => true,
    'title' => 'Home',
    "routeName" => "home"
];
?>


@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>

            <div class="panel-body">
                You are logged in!
                <a href="{{route('profile')}}">Click me!</a>
            </div>
        </div>
        <div class="well">
            <form>
                <select class="select-test">
                    <option>A</option>
                    <option>B</option>
                </select>
                <input type="text" class="selectize-test">
            </form>
        </div>
    </div>
</div>
@endsection


@section('component-script')
<script src="/js/home/index.js"></script>
@endsection('component-script')