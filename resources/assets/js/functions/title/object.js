/* 
 * Perhaps distinguish between global news providers(chat, forum, ...) and page-specific (if there will exist any) 
 */

module.exports = {
    originalTitle: document.title.replace(/&amp;/g, '&'),
    titleString: this.originalTitle,
    count: 0,
    newsRequesters: {

    },
    updateOriginalTitle: function(newTitle){
        this.originalTitle = newTitle.replace(/&amp;/g, '&');
        this.updateString();
    },
    updateString: function () {

        this.titleString = this.originalTitle;

        this.updateCount();
        if (this.count) {
            this.titleString = "(" + (this.count > 99 ? "99+" : this.count) + ") " + this.titleString;
        }
        document.title = this.titleString;
    },
    updateCount: function () {
        this.count = 0;
        for (var iii in this.newsRequesters) {
            this.count += this.newsRequesters[iii];
        }
    },
    /*
     * removeNewsRequest removes a provider from the list
     * @param {String} name of the provider
     * @param {int} initialCount 
     */
    removeNewsRequest: function (name) {
        if (!name)
            return false;
        if (this.newsRequesters.hasOwnProperty(name)) {
            delete this.newsRequesters[name];
            this.updateString();
        }
    },
    /*
     * addNewsRequest adds another provider that wants to publish their notifications
     * @param {String} name of the provider
     * @param {int} initialCount 
     */
    addNewsRequest: function (name, initialCount) {
        if (!name || !initialCount || initialCount < 0)
            return false;
        if (this.newsRequesters.hasOwnProperty(name)) {
            return this.updateNewsRequest(name, initialCount, true)
        }
        this.newsRequesters[name] = initialCount;
        this.updateString();
    },
    /*
     * updateNewsRequest updates a provider's count
     * @param {String} name  of the provider
     * @param {int} count 
     * @param {Boolean} override if this is set to true, the number of this provider will be overwritten, otherwise count will be added to the current value
     */
    updateNewsRequest: function (name, count = 0, override = true) {
        if (!name)
            return false;
        if (!count && override)
            return this.removeNewsRequest(name)
        if (!this.newsRequesters.hasOwnProperty(name)) {
            return this.addNewsRequest(name, count);
        }
        if (override)
            this.newsRequesters[name] = count;
        else
            this.newsRequesters[name] += count;


        if (this.newsRequesters[name] <= 0)
            this.removeNewsRequest(name);
        this.updateString();
    }


}
