module.exports = {
    escapeHtml: function (text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function (m) {
            return map[m];
        });
    },
    br2nl: function (str) {
        return str.replace(/<br\s*\/?>/mg, "\n");
    },
    nl2br: function (str) {
        return str.replace("\n", "<br/>");
    },
    escapeRegExp: function (s) {
        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    },
    href: {
        removeOrigin: function(href, origin){
            var _href, _origin;
            switch(arguments.length){
                case 0:
                    _href = location.href;
                    _origin = location.origin;
                    break;
                case 1:
                    _origin = location.origin;
                    _href = href;
                    break;
                default:
                    _origin = origin;
                    _href = href;
                                        
            }
            return _href.indexOf(_origin) == 0 ? _href.substr(_origin.length) : _href;
        },
        parse: function (href) {
            var protocol = href.split(":");

            var noProtocol = protocol.splice(1).join("");
            protocol = protocol[0];

            var webProtocols = ["https", "http", "file", "ftp"];
            var actionProtocols = ["javascript", "mailto", "", ""];
            if (webProtocols.indexOf(protocol) != -1) {
                var hash = noProtocol.split("#");
                noProtocol = hash[0];
                hash = href.indexOf("#") != -1 ? hash[1] : null;

                var params = noProtocol.split("/").splice(2);
                var domain = params.splice(0, 1)[0];
                return {
                    protocol: protocol,
                    domain: domain,
                    params: params,
                    hash: hash,
                    isHttp: ["https", "http"].indexOf(protocol) != -1
                }
            }
            else if (protocol == actionProtocols[0]) {
                return {
                    protocol: protocol,
                    action: noProtocol,
                }
            }
            else {
                return {
                    protocol: protocol,
                }
            }


        }
    },
};
