/* 
 * Exports object changePage
 */
module.exports = {

    execute: function (html, data) {
        $(".main-container").removeClass(window._data.routeName)
                .addClass(data.routeName);
        $(".main-container").html(html);
        titleHandling.updateOriginalTitle(data.title);
        window._data = data;
        $.material.init();
        //MathJax ? MathJax.Hub.Queue(["Typeset", MathJax.Hub, $(".main-container")[0]]) : null;
        this.scripts[data.routeName] ? this.scripts[data.routeName]() : null;
    },
    scripts: {

    },
    addScript: function (name, callback) {
        this.scripts[name] = callback;
        $(callback);//$(document).ready(callback);
    }
}
