/* 
 * Exports object historyInit
 */

module.exports = init;

redirect = require('./redirect');
changePage = require('./changePage');
parser = require('./../parser');




/*
 * history init
 * 
 */
function init() {
    var html = $(".main-container").html();
    var data = window._data;
    window.history.replaceState({"html": html, "data": data}, "", window.location.href);
    
}

$(document).ready(init); // tudi ob refreshu je treba



$(document).on("click", "a", function (e) {
    var href = this.href;
    console.log("Click!");
    if (parser.href.parse(href).isHttp && this.target != "_blank" && (!$(this).hasClass("prevent-redirect") || $(this).hasClass(".chat-link-page"))) { //chat link page je link na /messages strani, in čeprav ima class prevent redirect, je treba redirectad
        console.log("Going for redirect");
        e.preventDefault();
        redirect(href);

    }
    else {
        window.console.log("Not going for redirect");
    }
});

window.onpopstate = function (e) {
    if (e.state) {
        changePage.execute(e.state.html, e.state.data);
    }
};


