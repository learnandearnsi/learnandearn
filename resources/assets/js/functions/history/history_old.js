/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Exports function redirect
 */
module.exports = redirect;
function redirect(href, reload) {
    if (href != location.href || reload) {
        $.ajax({
            url: href,
            data: {
                _ajax: true,
            },
            success: function (response) {
                
                var xml = $(response);
                var data = JSON.parse(xml.find("data").text()),
                        title = data.title,
                        html = xml.find("content").html();
                
                console.log(data);
                console.log(html);
                
                if(data.authenticated != window._data.authenticated){
                    
                }

                
                $(".main-container").html(html);
                
                
                
                //MathJax ? MathJax.Hub.Queue(["Typeset", MathJax.Hub, $(".main-container")[0]]) : null;
                document.title = (title? title.replace(/&amp;/g, '&')+' - ' : '') + 'Learn and Earn';


                window.history.pushState({"html": html, "pageTitle": title}, "", href);
*/
            }
        });
    } else {
//        console.log("same link");
    }
}

var parser = require('./parser');

window.onpopstate = function (e) {
    //console.log("location: " + document.location);
    //console.log(e.state);
    if (e.state) {

        document.title = e.state.pageTitle.replace(/&amp;/g, '&');
        $(".main-container").html(e.state.html);
        
        //console.log(e.state.head);

    }
};
/*
function replaceHead(stringWithNewHead, callback, replaceAny) {
    //console.log(stringWithNewHead);
    var removable = $("script,link").not("[type='math/tex']").not("[type='math/tex; mode=display']").filter(function () {
        console.log(this);
        var domain = parseHref(this.href || this.src).domain;
        return domain == "www.learn-and-earn.net" || (location.hostname == "localhost" && domain == "localhost");
    });
    if (!replaceAny)
        removable.not(".universal");
    //console.log(removable);
    removable.remove();
    var div = $("<div>").append(stringWithNewHead);
    div.find("link").not(".universal").appendTo("head");
    callback ? callback() : null;
    div.find("script").not(".universal").appendTo("head");
    div.remove();
}
*/
$(document).on("click", "a", function (e) {
    var href = this.href;
    if (parser.parseHref(href).isHttp && this.target != "_blank" && (!$(this).hasClass("prevent-redirect") || $(this).hasClass(".chat-link-page"))) { //chat link page je link na /messages strani, in čeprav ima class prevent redirect, je treba redirectad
        e.preventDefault();
        redirect(href);

    }
});

function checkHistory() {
    var html = $(".main-container").html(),
            head = $("head").html();
    //console.log(html);
    var title = document.title;
    window.history.replaceState({"html": html, "pageTitle": title, "head": head}, "", window.location.href);
}

$(document).ready(checkHistory); // tudi ob refreshu je treba