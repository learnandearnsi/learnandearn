/* 
 * Exports function redirect
 */
module.exports = redirect;
changePage = require("./changePage");
parser = require('./../parser');

/*
 * A function for redirects using the history API
 * @param {String} href the href where to go
 * @param {Boolean} reload if this is set to true, function will not prevent a redirect to the same page
 */
function redirect(href, reload) {
    window.console.log("Redirect started");
    if (!_.isEqual(parser.href.parse(href).params, parser.href.parse(location.href).params) || reload) { //check if we are about to leave the page or the refresh has been approved
        $.ajax({
            url: href,
            data: {
                _ajax: true,
            },
            success: function (response) {

                var xml = $(response);
                var data = JSON.parse(xml.find("data").text()),
                        html = xml.find("content").html();

                /*console.log(data);
                 console.log(html);*/

                changePage.execute(html, data);
                window.history.pushState({"html": html, "data": data}, "", href);

            }
        });
    } else { //an unauthorized redirect to the current page has been requested
        //we do nothing
        window.console.log("Not authorized for refresh");
    }
}


