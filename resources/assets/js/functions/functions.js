/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//var root_path = "";
var MathJaxRegex = /<span[ ]*class="MathJax_Preview"[^>]*>[^<]*<\/span>\s*<(div|span)[^>]*>(?:.(?!script))*<script[ ]*type="math\/tex(?:;[ ]*mode=display)?"[ ]*id="[^"]*">(.*)<\/script>/gi;

function stayOnline() {
    $.ajax({
        method: 'POST',
        url: "/globalFunctions/stayOnline.php",
        success: function (response) {
            //console.log(response);
        }
    });
    setTimeout(stayOnline, 60000);
    //console.log("online again");
}
function reload() {
    redirect(location.href, true)
}
function ajaxParsing(response, object) {

    var data = $(response).find("data");
    var mainText = $(response).find("text").html();//response.replace(/\s<root>\s<text>(.*)<\/text>\s<data>(.*)<\/data>\s<\/root>\s/,"$1");
    //console.log($(response).find("text").html());

    var Error = data.find("error");
    var errnum = Error.find("num").text();

    var logObj = {
        length: 0,
        url: object.url,
        response: response,
        originalObject: object,
        data: data,
        mainText: mainText,
        logs: {}
    };
    data.find("logs").children("log").each(function () {
        var label = $(this).find("label").html();
        var message = $(this).find("message").html();
        var existing = logObj.logs[label];
        if (existing) {
            if (existing.push) {
                existing.push(message)
            }
            else {
                logObj.logs[label] = [existing, message];
            }
        }
        else {
            logObj.logs[label] = message;
        }
        logObj.length++;
    });

    var errObj = {
        num: errnum,
        text: Error.find("message").text()
    };
    var process = true;
    switch (errnum) {
        case "1":
            location.reload();
            break;
        case "":
            break;
        default :
            process = object[errnum] ? object[errnum](data, errObj, logObj) : true;
    }

    logObj.process = process;
    if (object.log)
        console.log(logObj);

    return {
        data: data,
        mainText: mainText,
        process: process,
        logObj: logObj,
        errObj: errObj
    };
}

$.ajaxSetup({
    url: "/testi/testAjax.php",
    error: function (jqXHR, text, error) {
        var that = this;
        var data = this.data;
        data.__ajax_resend_num = data.__ajax_resend_num ? data.__ajax_resend_num + 1 : 1;
        that.data = data;
        if (data.__ajax_resend_num < 5) {
            setTimeout(function () {
                $.ajax(that);
            }, 1000);
        }
        else {
            console.log("resend: " + that.url + "\n failed");
            console.log(jqXHR);
            console.log(jqXHR.statusCode());
            console.log(text + "\n" + error + "\n" + this.url);
        }
    }
});

function firstToUpper(id, str) {
    if (str === "")
        return;
    var space = true;
    var lastChar = -1;
    var newStr = "";
    while (space) {
        lastChar++;
        if (str[lastChar] !== " ") {
            newStr += str[lastChar].toUpperCase();
        }
        else
            continue;
        space = false;
        lastChar++;
        for (var iii = lastChar; iii < str.length; iii++) {
            if (str[iii] === " ") {
                newStr += " ";
                space = true;
                lastChar = iii;
                break;
            }
            else {
                newStr += str[iii].toLowerCase();
            }
        }

    }

    id.value = newStr;
}


function copyTextToClipboard(text) {
    var textArea = document.createElement("textarea");

    //
    // *** This styling is an extra step which is likely not required. ***
    //
    // Why is it here? To ensure:
    // 1. the element is able to have focus and selection.
    // 2. if element was to flash render it has minimal visual impact.
    // 3. less flakyness with selection and copying which **might** occur if
    //    the textarea element is not visible.
    //
    // The likelihood is the element won't even render, not even a flash,
    // so some of these are just precautions. However in IE the element
    // is visible whilst the popup box asking the user for permission for
    // the web page to copy to the clipboard.
    //

    // Place in top-left corner of screen regardless of scroll position.
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;

    // Ensure it has a small width and height. Setting to 1px / 1em
    // doesn't work as this gives a negative w/h on some browsers.
    textArea.style.width = '2em';
    textArea.style.height = '2em';

    // We don't need padding, reducing the size if it does flash render.
    textArea.style.padding = 0;

    // Clean up any borders.
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';

    // Avoid flash of white box if rendered for any reason.
    textArea.style.background = 'transparent';


    textArea.value = text;

    document.body.appendChild(textArea);

    textArea.select();

    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
    } catch (err) {
        console.log('Oops, unable to copy');
    }

    document.body.removeChild(textArea);
}




function readURL(input, onload) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = onload;
//                function (e) {
//            $('#blah').attr('src', e.target.result);
//        }

        reader.readAsDataURL(input.files[0]);
    }
}

function search(contextElement, searchedSelector, resultClass, textSelector, separateChildNodes) {


    separateChildNodes = typeof separateChildNodes !== 'undefined' ? separateChildNodes : true;
    if (!window.globalSearch)
        globalSearch = {
            log: false
        };

    var val = this.value;

    globalSearch.log ? console.log("search: " + val + "\n separatechildnodes: " + separateChildNodes) : null;
    globalSearch.log ? console.log(arguments) : null;

    var regex = [new RegExp(escapeRegExp('<span class="' + resultClass + ' search-result">') + '([^<]*)<\/span>', 'gi'), function () {
            globalSearch.log ? console.log(regex) : null;
            globalSearch.log ? console.log(arguments) : null;
            return arguments[1];
        }];
    var regex2 = [new RegExp(escapeRegExp(val), 'i'), function (x) {
            globalSearch.log ? console.log("x: " + x) : null;
            globalSearch.log ? console.log('<span class="' + resultClass + ' search-result">' + x + "<\/span>") : null;
            return '<span class="' + resultClass + ' search-result">' + x + "<\/span>"
        }];


    $(contextElement).find(searchedSelector).each(function () {
        if (typeof textSelector != "object") {
            textSelector = [textSelector];
        }
        for (var iii in textSelector) {

            globalSearch.log ? console.log(this) : null;
            globalSearch.log ? console.log(textSelector[iii]) : null;

            selObject = $(this).find(textSelector[iii]);
            globalSearch.log ? console.log(selObject) : null;

            if (!selObject.length)
                continue;
            var html = selObject.html();
            html = html.replace(regex[0], regex[1]);
            selObject.html(html);
            globalSearch.log ? console.log(selObject.html()) : null;
            globalSearch.log ? console.log(selObject.text().replace(regex2[0], regex2[1])) : null;
            //rekurzivno zamenjaj vse posebej
            if (val == "") {
                selObject.html(html ? html.replace("&nbsp;", " ") : null);
            }
            else if (separateChildNodes) {
                selObject.find("*").add(selObject).each(function () {
                    if ($(this).children().length > 0) {
                        return;
                    }
                    else {
                        var text = $(this).text();
                        $(this).html(text.replace(regex2[0], regex2[1]));
                    }
                });
            }
            else {
                selObject.html(html.replace(regex2[0], regex2[1]));

            }

            if (regex2[0].test(selObject.text()) || val == "")
                $(this).show();
            else
                $(this).hide();
        }
        globalSearch.log ? console.log("\n\n") : null;
    });
    globalSearch.log ? console.log("end\n________________________________________________\n________________________________________________") : null;
}



function inIframe() {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}