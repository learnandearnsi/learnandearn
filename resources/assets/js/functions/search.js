module.exports = function (contextElement, searchedSelector, resultClass, textSelector, separateChildNodes) {


    separateChildNodes = typeof separateChildNodes !== 'undefined' ? separateChildNodes : true;
    if (!window.globalSearch)
        globalSearch = {
            log: false
        };

    var val = this.value;

    globalSearch.log ? console.log("search: " + val + "\n separatechildnodes: " + separateChildNodes) : null;
    globalSearch.log ? console.log(arguments) : null;

    var regex = [new RegExp(escapeRegExp('<span class="' + resultClass + ' search-result">') + '([^<]*)<\/span>', 'gi'), function () {
            globalSearch.log ? console.log(regex) : null;
            globalSearch.log ? console.log(arguments) : null;
            return arguments[1];
        }];
    var regex2 = [new RegExp(escapeRegExp(val), 'i'), function (x) {
            globalSearch.log ? console.log("x: " + x) : null;
            globalSearch.log ? console.log('<span class="' + resultClass + ' search-result">' + x + "<\/span>") : null;
            return '<span class="' + resultClass + ' search-result">' + x + "<\/span>"
        }];


    $(contextElement).find(searchedSelector).each(function () {
        if (typeof textSelector != "object") {
            textSelector = [textSelector];
        }
        for (var iii in textSelector) {

            globalSearch.log ? console.log(this) : null;
            globalSearch.log ? console.log(textSelector[iii]) : null;

            selObject = $(this).find(textSelector[iii]);
            globalSearch.log ? console.log(selObject) : null;

            if (!selObject.length)
                continue;
            var html = selObject.html();
            html = html.replace(regex[0], regex[1]);
            selObject.html(html);
            globalSearch.log ? console.log(selObject.html()) : null;
            globalSearch.log ? console.log(selObject.text().replace(regex2[0], regex2[1])) : null;
            //rekurzivno zamenjaj vse posebej
            if (val == "") {
                selObject.html(html ? html.replace("&nbsp;", " ") : null);
            }
            else if (separateChildNodes) {
                selObject.find("*").add(selObject).each(function () {
                    if ($(this).children().length > 0) {
                        return;
                    }
                    else {
                        var text = $(this).text();
                        $(this).html(text.replace(regex2[0], regex2[1]));
                    }
                });
            }
            else {
                selObject.html(html.replace(regex2[0], regex2[1]));

            }

            if (regex2[0].test(selObject.text()) || val == "")
                $(this).show();
            else
                $(this).hide();
        }
        globalSearch.log ? console.log("\n\n") : null;
    });
    globalSearch.log ? console.log("end\n________________________________________________\n________________________________________________") : null;
}