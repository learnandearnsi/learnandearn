

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */
console.log("before jquery:");//console.log($);
window.$ = window.jQuery = require('jquery');
console.log("after jquery:");console.log($);


require('bootstrap-sass');
require('bootstrap-material-design/dist/js/material.js');
require('bootstrap-material-design/dist/js/ripples.js');
console.log("after bootstrap:");console.log($);

window._ = require('lodash');

require('select2');
//require('selectize');
$(function () {
    $.material.init();
});
console.log("after material init:");console.log($);

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

//window.Vue = require('vue');
////window.VueRouter = require('vue-router');
//
////window.Vue.use(window.VueRouter);

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */
/*
window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window._csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};
*/
/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from "laravel-echo"

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });
