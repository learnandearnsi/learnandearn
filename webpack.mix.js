var mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.autoload({
    jquery: ['$', 'window.jQuery',"jQuery","window.$","jquery","window.jquery"]
});

mix.js('resources/assets/js/app.js', 'public/js', {includePaths: ['node_modules/bootstrap-material-design/dist/js/']})
        .extract([/*'vue',*/ 'lodash', 'jquery', 'bootstrap-sass', 'bootstrap-material-design', 'axios', 'select2' ]);
/* .js('resources/assets/js/functions/functions.js', 'public/js/functions')
 .js('resources/assets/js/functions/history.js', 'public/js/functions')*/
//
//mix.sass('resources/assets/sass/app.scss', 'public/css', {includePaths: ['node_modules/bootstrap-sass/assets/stylesheets/']});
//.sass('resources/assets/css/profile/index.scss', 'public/css/profile');
//.sass('resources/assets/bower_components/Bootflat/bootflat/scss/bootflat.scss', 'public/css');
//mix.copyDirectory('resources/assets/js/preserve', 'public/js/');
